const socket=io('http://localhost:8000');

const form=document.getElementById('send-container');
const messageInp=document.getElementById('messageInp');
const messageContainer=document.querySelector('.container');
const audio=new Audio('messenger.mp3')

const append=(message,position)=>{
  const messageElement=document.createElement('div');
  messageElement.innerText=message;
  messageElement.classList.add('message');
  messageElement.classList.add(position);
  messageContainer.append(messageElement);
  if(position=='left'){
    audio.play();
  }
};

const name=prompt('Enter your name to join');

if(name!="" && name!= null && name!= undefined)
{
  form.addEventListener('submit',(e)=>{
    e.preventDefault();
    const message=messageInp.value;
    append(`You: ${message}`,'right');
    socket.emit('send',message);
    messageInp.value='';
  })

  socket.emit('new-user-joined',name);
  socket.on('User-Joined',name =>{
    append(`${name} joined the chat`,'right');
  });

  socket.on('recieve',data =>{
    append(`${data.name}: ${data.message}`,'left');
  });

  socket.on('left',name=>{
    append(`${name}: left the chat`,'right');
  })
}

else{
  alert('Please Reload page and Enter the Correct Name');
}
